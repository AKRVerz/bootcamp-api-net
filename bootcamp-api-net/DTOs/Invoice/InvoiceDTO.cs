namespace databaseapi.DTOs.Invoice
{
    public class InvoiceDTO
    {
        // untuk invoice
        public string noInvoice { get; set; }

        //public DateTime date { get; set; }

        public Guid idUserFK { get; set; }

        //public string namaPenjual { get; set; }

        public string payment_method { get; set; }
        public bool isPaid { get; set; }


    }
}
