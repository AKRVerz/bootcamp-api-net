﻿namespace bootcamp_api_net.DTOs.Course
{
    public class CourseDTO
    {
        public string Title { get; set; } = string.Empty;

        public string Description { get; set; }
        public int Price { get; set; }
        //public DateTime Schedule { get; set; } // Sesuaikan dengan kolom Schedule dalam tabel Courses
        public Guid MerkId { get; set; } // Sesuaikan dengan kolom MerkId dalam tabel Courses
        public byte[]? ImagePath { get; set; }

        public bool isActive { get; set; }

    }
}
