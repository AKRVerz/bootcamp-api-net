﻿namespace bootcamp_api_net.DTOs.User
{
    public class LoginResponseDTO
    {
        public string Token { get; set; } = string.Empty;
    }
}
