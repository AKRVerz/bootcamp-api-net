﻿namespace bootcamp_api_net.DTOs.Payment
{
    public class PaymentMethodDTO
    {
        public string Name { get; set; }

        public byte[]? Logo { get; set; }

        public bool Status { get; set; }
    }
}
