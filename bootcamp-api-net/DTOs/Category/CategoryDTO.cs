namespace databaseapi.DTOs.Category
{
    public class CategoryDTO
    {
        public string CategoryName { get; set; } = string.Empty;

        public string deskripsi { get; set; }

        public byte[]? ImagePath { get; set; }

        public bool isActive { get; set; }

        //public DateTime Created { get; set; } // Tambahkan properti Created

        //public DateTime Updated { get; set; } // Tambahkan properti Updated
    }
}
