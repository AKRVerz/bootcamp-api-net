﻿namespace bootcamp_api_net.DTOs.OrderDetail
{
    public class OrderDetailDTO
    {
        //public int Invoice_Id { get; set; }

        public string noInv { get; set; }

        public Guid id_User { get; set; }

        public int Schedule_Id { get; set; }

        public Guid Course_id { get; set; }
    }
}
