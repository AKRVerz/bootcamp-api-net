﻿namespace bootcamp_api_net.Models
{
    public class CourseUser
    {
        public Guid Id { get; set; }

        public Guid IdCourse { get; set; }

        public DateOnly Schedule { get; set; }

        public string Title { get; set; }

        public string Deskripsi { get; set; }
        public IFormFile ImageFile { get; set; }
        public string ImagePath { get; set; }

        public string Category { get; set; }

        //public string DescriptionCategory { get; set; }
    }
}
