﻿namespace bootcamp_api_net.Models
{
    public class PaymentMethod
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public byte[]? Logo { get; set; }

        public int Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
