﻿namespace bootcamp_api_net.Models
{
    public class OrderDetail
    {
        public int Id { get; set; }

        public Guid id_User { get; set; }

        public int Invoice_Id { get; set; }

        public string noInv { get; set; }

        public int Schedule_Id { get; set; }

        public Guid Course_id { get; set; }

        public DateOnly Schedule { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }
    }
}
