﻿namespace bootcamp_api_net.Models
{
    public class Cart
    {
        public int Id { get; set; }

        public Guid Id_User { get; set; }

        public int Id_Schedule { get; set; }

        public Guid Id_Course { get; set; }

        public DateOnly Jadwal { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public Guid MerkId { get; set; }

        public byte[]? ImagePath { get; set;}

    }
}
