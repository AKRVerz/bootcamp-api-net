﻿namespace bootcamp_api_net.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string? Email { get; set; }
        public bool? IsActivated { get; set; }

    }
}

