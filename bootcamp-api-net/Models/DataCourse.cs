﻿namespace bootcamp_api_net.Models
{
    public class DataCourse
    {
        public Guid Id { get; set; }
        public string Title { get; set; } = string.Empty;

        public DateOnly Jadwal { get; set; }
        public string Description { get; set; } = string.Empty;
        public int Price { get; set; }
        //public DateTime Schedule { get; set; } // Sesuaikan dengan kolom Schedule dalam tabel Courses
        public Guid MerkId { get; set; } // Sesuaikan dengan kolom MerkId dalam tabel Courses
        public byte[]? ImagePath { get; set; } // Sesuaikan dengan kolom ImagePath dalam tabel Courses

        public int isActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

    }
}
