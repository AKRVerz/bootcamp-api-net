namespace databaseapi.Model
{
    public class Category
    {
        public Guid Id { get; set; }
        public string CategoryName { get; set; }

        public string deskripsi { get; set; }
        public byte[]? ImagePath { get; set; }
        public int isActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
