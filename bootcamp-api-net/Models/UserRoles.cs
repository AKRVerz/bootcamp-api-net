﻿namespace bootcamp_api_net.Models
{
    public class UserRoles
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string Role { get; set; } = string.Empty;
    }
}

