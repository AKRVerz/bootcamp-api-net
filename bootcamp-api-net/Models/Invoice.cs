
using MySql.Data.Types;

namespace databaseapi.Model
{
    public class Invoice
    {
        public int idInvoice { get; set; }

        public Guid id_User { get; set; }

        public string noInvoice { get; set; }

        public DateTime date { get; set; }

        //public Guid idUserFK { get; set; }

        //public string namaPenjual { get; set; }

        public string payment_method { get; set; }

        public int isPaid { get; set; }

        //public List<InvoiceDetail> invoiceDetails { get; set; }
    }
}
