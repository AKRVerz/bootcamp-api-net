﻿using Microsoft.VisualBasic;

namespace bootcamp_api_net.Models
{
    public class Schedule
    {
        public int Id { get; set; }

        public DateOnly Jadwal { get; set; }

        public Guid courseFK { get; set; }

        public string Title { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;
        public int Price { get; set; }
        //public DateTime Schedule { get; set; } // Sesuaikan dengan kolom Schedule dalam tabel Courses
        public string ImagePath { get; set; } // Sesuaikan dengan kolom ImagePath dalam tabel Courses
    }
}
