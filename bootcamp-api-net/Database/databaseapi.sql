-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2023 at 03:14 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databaseapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` char(36) NOT NULL,
  `schedule_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categorycourses`
--

CREATE TABLE `categorycourses` (
  `id` char(36) NOT NULL,
  `categoryname` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `imagepath` varchar(255) DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categorycourses`
--

INSERT INTO `categorycourses` (`id`, `categoryname`, `deskripsi`, `imagepath`, `isActive`, `Created`, `Updated`) VALUES
('070885b0-e00f-45a2-b0ec-b245eb235ded', 'Menghitung', 'Pelajaran Menghitung', '/image/mtk.png', 1, '2023-09-27 18:13:51', '2023-09-27 18:13:51'),
('0c1e9d92-8fa7-43bc-b774-16f042f1f835', 'Kategori 3', '', '/gambar/kategori3.jpg', 1, '2023-09-27 17:24:57', '2023-09-20 14:19:58'),
('3a4e8e55-6e5b-4efc-9d6a-1a0b14e4f3d7', 'Menghafal', '', '/image/sejarah.jpg', 0, '2023-09-28 02:10:24', '2023-09-28 02:10:23'),
('7b1f919b-c5bb-4d6e-8f2f-92e3d76eef4a', 'Kategori 1', '', '/gambar/kategori1.jpg', 1, '2023-09-27 17:25:03', '2023-09-20 14:19:58'),
('7e850422-6c14-47cb-a47c-64109036ce5b', 'Waktu', 'Waktu yang berhenti', '/image/bla.png', 1, '2000-12-31 17:00:00', '2000-12-31 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `Id` char(36) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `Price` int(11) NOT NULL,
  `MerkId` char(36) NOT NULL,
  `ImagePath` varchar(255) DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`Id`, `Title`, `deskripsi`, `Price`, `MerkId`, `ImagePath`, `isActive`, `Created`, `Updated`) VALUES
('088ae33e-f49a-40ef-8a2c-d849d5c9235a', 'Belajar', 'Muantep', 10870, '3a4e8e55-6e5b-4efc-9d6a-1a0b14e4f3d7', '', 1, '2023-09-14 14:16:54', '2023-09-28 12:23:49'),
('8d001dd7-75dc-4837-bc6c-dacc4d032d5f', 'Bebas', 'Gaya Bebas', 1000, '0c1e9d92-8fa7-43bc-b774-16f042f1f835', '/image/bla.png', 1, '2023-09-28 12:22:33', '2023-09-28 12:22:33'),
('92b102f6-7fa3-46d8-8abd-4d37ab5510f7', 'string2', 'Souls', 220, '3a4e8e55-6e5b-4efc-9d6a-1a0b14e4f3d7', NULL, 1, '2023-09-14 14:19:25', '2023-09-14 14:19:25'),
('a2b369eb-7e84-4a85-9d45-318aa98deca4', 'Kursus C', 'Sporti', 80, '0c1e9d92-8fa7-43bc-b774-16f042f1f835', '/gambar/kursusC.jpg', 1, '2023-09-14 11:15:00', '2023-09-28 08:50:37'),
('d2492a0f-bcbf-4823-8dab-7473b3ed078e', 'Kursus A', 'Offroad', 100, '7b1f919b-c5bb-4d6e-8f2f-92e3d76eef4a', '/gambar/kursusA.jpg', 1, '2023-09-14 08:00:00', '2023-09-14 08:30:00'),
('f8fe7e62-e1b8-4c56-aacf-0a5c8768d789', 'Kursus B', 'BestDealer', 150, '3a4e8e55-6e5b-4efc-9d6a-1a0b14e4f3d7', '/gambar/kursusB.jpg', 1, '2023-09-14 09:30:00', '2023-09-14 10:45:00');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `idInvoice` int(11) NOT NULL,
  `id_User` char(36) NOT NULL,
  `noInvoice` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `payment_method` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`idInvoice`, `id_User`, `noInvoice`, `status`, `date`, `payment_method`) VALUES
(1, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'WDJHUYY7867', 1, '2023-09-22 06:16:37', 'Go Pay'),
(2, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'OUIHWD6767', 1, '2023-09-29 21:10:34', 'Gopay'),
(5, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'UWIDGW676', 1, '2023-10-01 09:58:48', 'Ovo'),
(6, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'UIWHDW67', 1, '2023-10-02 16:06:46', 'Dana'),
(7, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'IUHIU67676', 1, '2023-10-02 19:00:24', 'Dana'),
(10, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'OUIGHIUWD6767', 1, '2023-10-02 19:55:03', 'BNI');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE `orderdetail` (
  `id` int(11) NOT NULL,
  `id_user` char(36) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `course_id` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`id`, `id_user`, `invoice_id`, `schedule_id`, `course_id`) VALUES
(1, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 1, 1, '92b102f6-7fa3-46d8-8abd-4d37ab5510f7'),
(2, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 1, 2, 'd2492a0f-bcbf-4823-8dab-7473b3ed078e'),
(3, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 2, 1, '8d001dd7-75dc-4837-bc6c-dacc4d032d5f'),
(5, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 2, 2, '8d001dd7-75dc-4837-bc6c-dacc4d032d5f'),
(6, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 2, 2, '8d001dd7-75dc-4837-bc6c-dacc4d032d5f'),
(7, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 2, 2, '8d001dd7-75dc-4837-bc6c-dacc4d032d5f'),
(8, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 10, 2, '088ae33e-f49a-40ef-8a2c-d849d5c9235a'),
(10, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 10, 4, '088ae33e-f49a-40ef-8a2c-d849d5c9235a');

-- --------------------------------------------------------

--
-- Table structure for table `paymentmethod`
--

CREATE TABLE `paymentmethod` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paymentmethod`
--

INSERT INTO `paymentmethod` (`id`, `name`, `logo`, `status`, `Created`, `Updated`) VALUES
(1, 'Bca', '/image/bca.jpg', 1, '2023-09-28 09:30:52', '2023-09-28 09:49:43');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` int(11) NOT NULL,
  `schedule` date NOT NULL,
  `course_id` char(36) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `schedule`, `course_id`, `created`, `updated`) VALUES
(1, '2023-09-20', '92b102f6-7fa3-46d8-8abd-4d37ab5510f7', '2023-09-20 14:45:25', '2023-09-20 14:45:25'),
(2, '2023-09-20', '088ae33e-f49a-40ef-8a2c-d849d5c9235a', '2023-09-20 14:55:26', '2023-09-20 14:55:26'),
(3, '2023-09-22', 'a2b369eb-7e84-4a85-9d45-318aa98deca4', '2023-09-21 21:58:47', '2023-09-21 21:58:47'),
(4, '2023-09-26', '088ae33e-f49a-40ef-8a2c-d849d5c9235a', '2023-09-26 16:35:09', '2023-09-26 16:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles` (
  `Id` int(11) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`Id`, `UserId`, `Role`) VALUES
(1, '9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` char(36) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `IsActivated` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `Username`, `Password`, `Email`, `IsActivated`) VALUES
('9c4f879c-6620-418d-b954-64a4a6dc2e4a', 'admin', '$2a$11$Jz5LA2Mrr57SzybX3iWxNe4h7cdAM3S6pGHTts/nmQ5lLdQ47qNri', 'replayer71@gmail.com', b'1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ScheduleFK` (`schedule_id`),
  ADD KEY `UserFK` (`user_id`);

--
-- Indexes for table `categorycourses`
--
ALTER TABLE `categorycourses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Merk_CategoryCourse` (`MerkId`);

--
-- Indexes for table `invoice`
  ADD KEY `FK_USER` (`id_User`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scheduleFK_Id` (`schedule_id`),
  ADD KEY `coursesFK_Id` (`course_id`),
  ADD KEY `InvoiceFK_Id` (`invoice_id`),
  ADD KEY `idUserFK` (`id_user`);

--
-- Indexes for table `paymentmethod`
--
ALTER TABLE `paymentmethod`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CourseFK` (`course_id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
  ADD PRIMARY KEY (`Id`);
--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `idInvoice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `paymentmethod`
--
ALTER TABLE `paymentmethod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `ScheduleFK` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`),
  ADD CONSTRAINT `UserFK` FOREIGN KEY (`user_id`) REFERENCES `users` (`Id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `FK_Merk_CategoryCourse` FOREIGN KEY (`MerkId`) REFERENCES `categorycourses` (`id`);

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `FK_USER` FOREIGN KEY (`id_User`) REFERENCES `users` (`Id`);

--
-- Constraints for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `InvoiceFK_Id` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`idInvoice`),
  ADD CONSTRAINT `coursesFK_Id` FOREIGN KEY (`course_id`) REFERENCES `courses` (`Id`),
  ADD CONSTRAINT `idUserFK` FOREIGN KEY (`id_user`) REFERENCES `users` (`Id`),
  ADD CONSTRAINT `scheduleFK_Id` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `CourseFK` FOREIGN KEY (`course_id`) REFERENCES `courses` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
