﻿namespace bootcamp_api_net.Emails.Template
{
    public class ActivationModel
    {
        public string? Email { get; set; }
        public string? Link { get; set; }
    }
}
