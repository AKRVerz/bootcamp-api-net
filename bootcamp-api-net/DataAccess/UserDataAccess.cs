﻿using bootcamp_api_net.Models;
using MySql.Data.MySqlClient;

namespace bootcamp_api_net.DataAccess
{
    public class UserDataAccess
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public UserDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        private MySqlConnection OpenConnection()
        {
            MySqlConnection connection = new MySqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
        #region  Single SQL Command
        //Previous statement will be overridden by the last statement
        //public bool CreateUserAccount(User user, UserRole userRole)
        //{
        //    bool result = false;

        //    using (MySqlConnection connection = new MySqlConnection(_connectionString))
        //    {
        //        using (MySqlCommand command = new MySqlCommand())
        //        {
        //            command.Connection = connection;
        //            command.Parameters.Clear();

        //            command.CommandText = "INSERT INTO Users (Id, Username, Password) VALUES (@id, @username, @password)";

        //            command.Parameters.AddWithValue("@id", user.Id);
        //            command.Parameters.AddWithValue("@username", user.Username);
        //            command.Parameters.AddWithValue("@password", user.Password);


        //            command.CommandText = "INSERT INTO UserRoles (UserId, Role) VALUES (@userId, @role)";

        //            command.Parameters.AddWithValue("@userId", userRole.UserId);
        //            command.Parameters.AddWithValue("@role", userRole.Role);

        //            try
        //            {
        //                connection.Open();

        //                int execResult = command.ExecuteNonQuery();

        //                result = execResult > 0 ? true : false;
        //            }
        //            catch
        //            {
        //                throw;
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }

        //    return result;
        //    //}
        #endregion

        #region Multiple Sql command (without transaction)
        // If first statement No OK, then next statment will be No OK
        // If first statment OK, then next statement No Ok, only first statement will be inserted

        //public bool CreateUserAccount(User user, UserRole userRole)
        //{
        //    bool result = false;

        //    using (MySqlConnection connection = new MySqlConnection(_connectionString))
        //    {
        //        MySqlCommand command1 = new MySqlCommand();
        //        command1.Connection = connection;
        //        command1.Parameters.Clear();

        //        command1.CommandText = "INSERT INTO Users (Id, Username, Password) VALUES (@id, @username, @password)";
        //        command1.Parameters.AddWithValue("@id", user.Id);
        //        command1.Parameters.AddWithValue("@username", user.Username);
        //        command1.Parameters.AddWithValue("@password", user.Password);


        //        MySqlCommand command2 = new MySqlCommand();
        //        command2.Connection = connection;
        //        command2.Parameters.Clear();

        //        command2.CommandText = "INSERT INTO UserRoles (UserId, Role) VALUES (@userId, @role)";
        //        command2.Parameters.AddWithValue("@userId", userRole.UserId);
        //        command2.Parameters.AddWithValue("@role", userRole.Role);

        //        try
        //        {
        //            connection.Open();
        //            var result1 = command1.ExecuteNonQuery();
        //            var result2 = command2.ExecuteNonQuery();

        //            if (result1 > 0 && result2 > 0)
        //                result = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            throw;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //    return result;
        //}
        #endregion

        #region Multiple Sql command (with transaction)
        public bool CreateUserAccount(User user, UserRole userRole)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                // open connection before begin transaction
                connection.Open();

                MySqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    MySqlCommand command1 = new MySqlCommand();
                    command1.Connection = connection;
                    command1.Transaction = transaction;
                    command1.Parameters.Clear();

                    command1.CommandText = "INSERT INTO Users (Id, Username, Password, Email, IsActivated) VALUES (@id, @username, @password, @email, @isActivated)";
                    command1.Parameters.AddWithValue("@id", user.Id);
                    command1.Parameters.AddWithValue("@username", user.Username);
                    command1.Parameters.AddWithValue("@password", user.Password);
                    command1.Parameters.AddWithValue("@email", user.Email);
                    command1.Parameters.AddWithValue("@isActivated", user.IsActivated);


                    MySqlCommand command2 = new MySqlCommand();
                    command2.Connection = connection;
                    command2.Transaction = transaction;
                    command2.Parameters.Clear();

                    command2.CommandText = "INSERT INTO UserRoles (Id,UserId, Role) VALUES (DEFAULT, @userId, @role)";
                    command2.Parameters.AddWithValue("@userId", userRole.UserId);
                    command2.Parameters.AddWithValue("@role", userRole.Role);


                    var result1 = command1.ExecuteNonQuery();
                    var result2 = command2.ExecuteNonQuery();

                    transaction.Commit();

                    result = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }
        #endregion
        public List<User> GetAllUsers()
        {
            List<User> user = new List<User>();

            string query = "SELECT * FROM `users` ";
            var temp = "";
            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                user.Add(new User
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    Username = reader["Username"].ToString() ?? string.Empty,
                                    Email = reader["Email"].ToString() ?? string.Empty,
                                    IsActivated = reader.GetBoolean(reader.GetOrdinal("IsActivated"))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close ();
                }
            }
            return user;
        }

        public List<User> GetAllUsersById(Guid id)
        {
            List<User> users = new List<User>();
            string query = "SELECT * FROM users WHERE Id = @Id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    Username = reader["Username"].ToString() ?? string.Empty,
                                    Email = reader["Email"].ToString() ?? string.Empty,
                                    IsActivated = reader.GetBoolean(reader.GetOrdinal("IsActivated"))
                                });
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return users;
        }

        public User GetUsersById(Guid id)
        {
            User users = null;
            string query = "SELECT * FROM users WHERE Id = @Id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users = new User
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    Username = reader["Username"].ToString() ?? string.Empty,
                                    Email = reader["Email"].ToString() ?? string.Empty,
                                    Password = reader["Password"].ToString() ?? string.Empty,
                                    IsActivated = reader.GetBoolean(reader.GetOrdinal("IsActivated"))
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return users;
        }


        public User GetCourseByUsername(string username)
        {
            User user = null;
            string query = "SELECT * FROM users WHERE Username = @Username";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        username = username.ToLower();
                        command.Parameters.AddWithValue("@Username", username);

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                user = new User
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    Username = reader["Username"].ToString() ?? string.Empty,
                                    Email = reader["Email"].ToString() ?? string.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return user;

        }

        public User? CheckUser(string username)
        {
            User? user = null;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT * From Users WHERE Username = @username";

                    command.Parameters.Clear();

                    command.Parameters.AddWithValue("@username", username);

                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                Username = reader["Username"].ToString() ?? string.Empty,
                                Password = reader["Password"].ToString() ?? string.Empty,
                                IsActivated = reader.GetBoolean(reader.GetOrdinal("IsActivated"))
                            };
                        }
                    }

                    connection.Close();

                }
            }

            return user;
        }

        public UserRole? GetUserRole(Guid userId)
        {
            UserRole? userRole = null;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();

                    command.CommandText = "SELECT * FROM UserRoles WHERE UserId = @userId";
                    command.Parameters.AddWithValue("@userId", userId);


                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            userRole = new UserRole
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                UserId = Guid.Parse(reader["UserId"].ToString() ?? string.Empty),
                                Role = reader["Role"].ToString() ?? string.Empty
                            };

                        }
                    }

                    connection.Close();

                }
            }

            return userRole;
        }

        public bool AcitvateUser(Guid id)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                MySqlCommand command = new MySqlCommand();
                command.Connection = connection;
                command.Parameters.Clear();

                command.CommandText = "UPDATE Users SET IsActivated = TRUE WHERE Id = @id";
                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();

                    result = command.ExecuteNonQuery() > 0;
                }
                catch
                {
                    throw;
                }
                finally { connection.Close(); }
            }

            return result;
        }

        public bool ResetPassword(string email, string password)
        {
            bool result = false;

            string query = "UPDATE Users SET Password = @Password WHERE Email = @Email";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();

                    command.CommandText = query;

                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@Password", password);

                    connection.Open();

                    result = command.ExecuteNonQuery() > 0 ? true : false;

                    connection.Close();
                }
            }

            return result;
        }

        public bool UpdateUser(Guid id, User user)
        {
            bool result = false;
            string query = "UPDATE Users SET Username = @user, Password = @password , Email = @email WHERE Id = @Id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        command.Parameters.AddWithValue("@user", user.Username);
                        command.Parameters.AddWithValue("@password", user.Password);
                        command.Parameters.AddWithValue("@email", user.Email);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool UpdateUserActive(Guid id)
        {
            bool result = false;
            string query = "UPDATE Users SET IsActivated = 1 WHERE Id = @Id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool UpdateUserNotActive(Guid id)
        {
            bool result = false;
            string query = "UPDATE Users SET IsActivated = 0 WHERE Id = @Id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public List<CourseUser> GetAllCourseUser(Guid Id) 
        {
            List<CourseUser> users = new List<CourseUser>();
            string query = "SELECT * FROM users " +
                "inner join orderdetail on users.Id = orderdetail.id_user " +
                "inner join schedule on orderdetail.schedule_id = schedule.id " +
                "inner join courses on schedule.course_id = courses.IdCourse " +
                "inner join categorycourses on courses.MerkId = categorycourses.id " +
                "WHERE Id = @Id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", Id);
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new CourseUser
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    IdCourse = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString(),
                                    Deskripsi = reader["deskripsi"].ToString(),
                                    Schedule = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Category = reader["categoryname"].ToString(),
                                    ImagePath = reader["ImagePath"].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return users;
        }

        public CourseUser GetCourseUser(Guid Id, Guid IdCourse)
        {
            CourseUser users = null;
            string query = "SELECT * FROM users " +
                "inner join orderdetail on users.Id = orderdetail.id_user " +
                "inner join schedule on orderdetail.schedule_id = schedule.id " +
                "inner join courses on schedule.course_id = courses.IdCourse " +
                "inner join categorycourses on courses.MerkId = categorycourses.id " +
                "WHERE users.Id = @Id and courses.IdCourse = @IdCourse";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", Id);
                        command.Parameters.AddWithValue("@IdCourse", IdCourse);
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users = new CourseUser
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    IdCourse = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString(),
                                    Deskripsi = reader["deskripsi"].ToString(),
                                    Schedule = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Category = reader["categoryname"].ToString(),
                                    ImagePath = reader["ImagePath"].ToString()
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return users;
        }
    }
}
