using databaseapi.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace databaseapi.DataAccess
{
    public class CategoryDataAccess
    {
        private readonly string _connectionString = "server=localhost;port=3306;database=databaseapi;user=root;password=";

        public List<Category> GetAllCategories()
        {
            List<Category> categories = new List<Category>();

            string query = "SELECT * FROM CategoryCourses";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                categories.Add(new Category
                                {
                                    Id = Guid.Parse(reader["ID"].ToString() ?? string.Empty),
                                    CategoryName = reader["CategoryName"].ToString() ?? string.Empty,
                                    ImagePath = (byte[])reader["ImagePath"],
                                    deskripsi = reader["deskripsi"].ToString(),
                                    isActive = Convert.ToInt32(reader["isActive"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Tangani exception di sini, contohnya, logika pesan error.
                    throw ex;
                }
            }

            return categories;
        }

        // Metode GetAllCategoriesActive() disini tidak perlu diubah karena sudah benar.

        public Category? GetCategoryById(Guid id)
        {
            Category? category = null;

            string query = $"SELECT * FROM CategoryCourses WHERE ID = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                category = new Category
                                {
                                    Id = Guid.Parse(reader["ID"].ToString() ?? string.Empty),
                                    CategoryName = reader["CategoryName"].ToString() ?? string.Empty,
                                    ImagePath = (byte[])reader["ImagePath"],
                                    deskripsi = reader["deskripsi"].ToString(),
                                    isActive = Convert.ToInt32(reader["isActive"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Tangani exception di sini, contohnya, logika pesan error.
                    throw ex;
                }
            }

            return category;
        }

        public bool InsertCategory(Category category)
        {
            bool result = false;

            string query = "INSERT INTO CategoryCourses (ID, CategoryName, deskripsi, ImagePath, isActive, Created, Updated) " +
                           "VALUES (@Id, @CategoryName, @deskripsi, @ImagePath, @isActive, @Created, @Updated)";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", category.Id);
                        command.Parameters.AddWithValue("@CategoryName", category.CategoryName);
                        command.Parameters.AddWithValue("@deskripsi", category.deskripsi);
                        command.Parameters.AddWithValue("@ImagePath", category.ImagePath);
                        command.Parameters.AddWithValue("@isActive", category.isActive);
                        command.Parameters.AddWithValue("@Created", DateTime.Now);
                        command.Parameters.AddWithValue("@Updated", DateTime.Now);

                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Tangani exception di sini, contohnya, logika pesan error.
                    throw ex;
                }
            }

            return result;
        }

        public bool UpdateCategory(Guid id, Category category)
        {
            bool result = false;

            string query = "UPDATE CategoryCourses SET CategoryName = @CategoryName, deskripsi = @deskripsi, " +
                           "ImagePath = @ImagePath, isActive = @isActive, Updated = @Updated WHERE ID = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        command.Parameters.AddWithValue("@CategoryName", category.CategoryName);
                        command.Parameters.AddWithValue("@deskripsi", category.deskripsi);
                        command.Parameters.AddWithValue("@ImagePath", category.ImagePath);
                        command.Parameters.AddWithValue("@isActive", category.isActive);
                        command.Parameters.AddWithValue("@Updated", DateTime.Now);

                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Tangani exception di sini, contohnya, logika pesan error.
                    throw ex;
                }
            }

            return result;
        }

        public bool ChangeCategoryStatus(Guid id, bool isActive)
        {
            bool result = false;

            string query = "UPDATE CategoryCourses SET isActive = @isActive, Updated = @Updated WHERE ID = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        command.Parameters.AddWithValue("@isActive", isActive);
                        command.Parameters.AddWithValue("@Updated", DateTime.Now);

                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Tangani exception di sini, contohnya, logika pesan error.
                    throw ex;
                }
            }

            return result;
        }

        public bool DeleteCategory(Guid id)
        {
            bool result = false;

            string query = "DELETE FROM CategoryCourses WHERE ID = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);

                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Tangani exception di sini, contohnya, logika pesan error.
                    throw ex;
                }
            }

            return result;
        }
        public List<Category> GetAllCategoriesActive()
        {
            List<Category> activeCategories = new List<Category>();

            string query = "SELECT * FROM CategoryCourses WHERE isActive = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                activeCategories.Add(new Category
                                {
                                    Id = Guid.Parse(reader["ID"].ToString() ?? string.Empty),
                                    CategoryName = reader["CategoryName"].ToString() ?? string.Empty,
                                    ImagePath = (byte[])reader["ImagePath"],
                                    deskripsi = reader["deskripsi"].ToString(),
                                    isActive = Convert.ToInt32(reader["isActive"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return activeCategories;
        }

        public List<Category> GetActiveCategories()
        {
            List<Category> activeCategories = new List<Category>();

            string query = "SELECT * FROM CategoryCourses WHERE isActive = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                activeCategories.Add(new Category
                                {
                                    Id = Guid.Parse(reader["ID"].ToString() ?? string.Empty),
                                    CategoryName = reader["CategoryName"].ToString() ?? string.Empty,
                                    ImagePath = (byte[])reader["ImagePath"],
                                    deskripsi = reader["deskripsi"].ToString(),
                                    isActive = Convert.ToInt32(reader["isActive"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return activeCategories;
        }

    }
}
