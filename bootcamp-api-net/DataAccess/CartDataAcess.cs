﻿using bootcamp_api_net.Models;
using MySql.Data.MySqlClient;

namespace bootcamp_api_net.DataAccess
{
    public class CartDataAcess
    {
        private readonly string _connectionString;

        public CartDataAcess(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private MySqlConnection OpenConnection()
        {
            MySqlConnection connection = new MySqlConnection(_connectionString);
            connection.Open();
            return connection;
        }

        public List<Cart> GetAllCart()
        {
            List<Cart> cart = new List<Cart>();
            string query = "SELECT * FROM cart " +
                "inner join schedule on cart.schedule_id = schedule.id "+
                "inner join courses on schedule.course_id = courses.IdCourse";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                cart.Add(new Cart
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Id_User = Guid.Parse(reader["user_id"].ToString() ?? string.Empty),
                                    Id_Schedule = Convert.ToInt32(reader["schedule_id"]),
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Id_Course = Guid.Parse(reader["course_id"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString(),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    Description = reader["deskripsi"].ToString(),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString()),
                                    ImagePath = (byte[])reader["ImagePath"],
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return cart;
        }
        public List<Cart> GetCartItemsByUserId(Guid userId)
        {
            List<Cart> cartItems = new List<Cart>();
            string query = @"SELECT cart.id, cart.user_id, cart.schedule_id, 
                                    schedule.schedule, courses.Title, courses.Price, courses.deskripsi, 
                                    courses.MerkId, courses.ImagePath
                             FROM cart
                             INNER JOIN schedule ON cart.schedule_id = schedule.id
                             INNER JOIN courses ON schedule.course_id = courses.IdCourse
                             WHERE cart.user_id = @User_id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@User_id", userId);

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                cartItems.Add(new Cart
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Id_User = Guid.Parse(reader["User_id"].ToString() ?? string.Empty),
                                    Id_Schedule = Convert.ToInt32(reader["schedule_id"]),
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Title = reader["Title"].ToString(),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    Description = reader["deskripsi"].ToString(),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString()),
                                    ImagePath = (byte[])reader["ImagePath"],
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return cartItems;
        }
        public bool InsertCart(Cart cart)
        {
            bool result = false;
            string query = "INSERT INTO cart (id, user_id,schedule_id) " +
                           "VALUES (@Id, @User_id,@Schedule_id)";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", cart.Id);
                        command.Parameters.AddWithValue("@User_id", cart.Id_User);
                        command.Parameters.AddWithValue("@Schedule_id", cart.Id_Schedule);

                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool DeleteCart(int[] id)
        {
            bool result = false;
            

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    for (int i=0;i<id.Length;i++) 
                    {
                        string query = "DELETE FROM cart WHERE Id in (@Id)";
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@Id", id[i]);

                            result = command.ExecuteNonQuery() > 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool DeleteAllCartbyUser(Guid idUser)
        {
            bool result = false;
            string query = "Delete from cart where user_id = @idUser";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@idUser", idUser);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }
    }
}
