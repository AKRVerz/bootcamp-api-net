using bootcamp_api_net.Models;
using databaseapi.Model;
using MySql.Data.MySqlClient;

namespace databaseapi.DataAccess
{
    public class InvoiceDataAccess
    {
        private readonly string _connectionString; //"server=localhost;port=3306;database=databaseapi;user=root;password= ;pooling = false; convert zero datetime=True";
        private readonly IConfiguration _configuration;
        private MySqlConnection conn;

        public InvoiceDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private MySqlConnection OpenConnection()
        {
            MySqlConnection connection = new MySqlConnection(_connectionString);
            connection.Open();
            return connection;
        }

        public List<Invoice> GetAll()
        {
            List<Invoice> invoice = new List<Invoice>();

            string query = "Select * from invoice";

            using (conn = new MySqlConnection(_connectionString)) ;
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    try
                    {
                        conn.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoice.Add(new Invoice
                                {
                                    // for invoice
                                    idInvoice = Convert.ToInt32(reader["idInvoice"]),
                                    id_User = Guid.Parse(reader["id_User"].ToString() ?? string.Empty),
                                    noInvoice = (reader["noInvoice"].ToString() ?? string.Empty),
                                    isPaid = Convert.ToInt32(reader["status"]),
                                    date = Convert.ToDateTime(reader["date"]),
                                    payment_method = reader["payment_method"].ToString()       
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return invoice;
        }


        //Get All Invoice berdasarkan user
        public List<Invoice> GetAllByUser(Guid id)
        {
            List<Invoice> invoice = new List<Invoice>();

            string query = $"Select * from invoice where id_User = @id";

            using (conn = new MySqlConnection(_connectionString)) ;
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    command.Connection = conn;
                    command.Parameters.Clear();

                    command.CommandText = query;
                    command.Parameters.AddWithValue("@id", id);
                    try
                    {
                        conn.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoice.Add(new Invoice
                                {
                                    // for invoice
                                    idInvoice = Convert.ToInt32(reader["idInvoice"]),
                                    id_User = Guid.Parse(reader["id_User"].ToString() ?? string.Empty),
                                    noInvoice = (reader["noInvoice"].ToString() ?? string.Empty),
                                    isPaid = Convert.ToInt32(reader["status"]),
                                    date = Convert.ToDateTime(reader["date"]),
                                    payment_method = reader["payment_method"].ToString()
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return invoice;
        }

        public Invoice? GetLastId()
        {
            Invoice invoice = null;

            string query = "SELECT Max(idInvoice) as 'idInvoice' from invoice";

            using (conn = new MySqlConnection(_connectionString)) ;
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    try
                    {
                        conn.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoice = new Invoice
                                {
                                    // for invoice
                                    idInvoice = Convert.ToInt32(reader["idInvoice"]),
                                    //noInvoice = (reader["noInvoice"].ToString() ?? string.Empty),
                                    //isPaid = Convert.ToInt32(reader["status"]),
                                    //date = Convert.ToDateTime(reader["date"]),
                                    //payment_method = reader["payment_method"].ToString()
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return invoice;
        }
        public Invoice? GetById(int idInvoice)
        {
            Invoice invoice = null;

            string query = $"Select * from invoice where idInvoice = @idInvoice";

            using (conn = new MySqlConnection(_connectionString)) ;
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    command.Connection = conn;
                    command.Parameters.Clear();

                    command.CommandText = query;
                    command.Parameters.AddWithValue("@idInvoice", idInvoice);

                    try
                    {
                        conn.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                invoice = new Invoice
                                {
                                    idInvoice = Convert.ToInt16(reader["idInvoice"]),
                                    noInvoice = (reader["noInvoice"]).ToString(),
                                    date = Convert.ToDateTime(reader["date"]),
                                    payment_method = reader["payment_method"].ToString(),
                                    isPaid = Convert.ToInt32(reader["status"])

                                };


                            }
                        }
                    }
                    catch (Exception ex) { throw ex; }
                    finally { conn.Close(); }
                }
            }

            return invoice;
        }
        public List<OrderDetail> GetByIdDetail(Guid id, int idInvoice)
        {
            List<OrderDetail> orderDetail = new List<OrderDetail>();

            string query = $"Select * from orderdetail inner join schedule on orderdetail.schedule_id = schedule.id " +
                $"inner join courses on orderdetail.course_id = courses.IdCourse" +
                $" where id_user = @id and invoice_id = @IdInvoice";

            using (conn = new MySqlConnection(_connectionString)) ;
            {
                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    command.Connection = conn;
                    command.Parameters.Clear();

                    command.CommandText = query;
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@IdInvoice", idInvoice);
                    try
                    {
                        conn.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                orderDetail.Add(new OrderDetail
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    id_User = Guid.Parse(reader["id_user"].ToString() ?? string.Empty),
                                    Invoice_Id = Convert.ToInt32(reader["invoice_id"]),
                                    Schedule_Id = Convert.ToInt32(reader["schedule_id"]),
                                    Course_id = Guid.Parse(reader["course_id"].ToString()),
                                    Schedule = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Title = reader["Title"].ToString(),
                                    Description = reader["deskripsi"].ToString(),
                                    Price = Convert.ToInt32(reader["Price"])
                                });
                            }
                        }
                    }
                    catch (Exception ex) { throw ex; }
                    finally { conn.Close(); }
                }
            }

            return orderDetail;
        }



        public bool Insert(Invoice invoice)
        {
            bool result = false;
            string date = invoice.date.ToString("yyyy-MM-dd HH:mm:ss");

            // untuk invoice
            // sudah bayar (true) = 0
            // Jika belum (false) = 1
            string query = $"INSERT INTO invoice(idInvoice,id_User,noInvoice, status, date,payment_method) " +
              $"VALUES (@idInvoice, @idUser , @noInvoice, @status, @date, @payment_method)";
            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    // insert invoice
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@idInvoice", invoice.idInvoice);
                        command.Parameters.AddWithValue("@idUser", invoice.id_User);
                        command.Parameters.AddWithValue("@noInvoice", invoice.noInvoice);
                        command.Parameters.AddWithValue("@status", invoice.isPaid);
                        command.Parameters.AddWithValue("@date", date);
                        command.Parameters.AddWithValue("@payment_method", invoice.payment_method);
                        result = command.ExecuteNonQuery() > 0 ? true : false;


                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool InsertDetail(OrderDetail orderDetail)
        {
            bool result = false;
            // untuk invoiceDetail
            string query = $"INSERT INTO orderdetail( id_user,invoice_id, schedule_id,course_id) " +
               $"VALUES (@IdUserFk,(Select idInvoice from invoice where noInvoice = @noInvoice),@idScheduleFK,@idCourseFK)";
            using (MySqlConnection connection = OpenConnection())
            {
                // insert invoice
                try
                {
                    using (MySqlCommand command = new MySqlCommand())
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@IdUserFk", orderDetail.id_User);
                        command.Parameters.AddWithValue("@noInvoice", orderDetail.noInv);
                        command.Parameters.AddWithValue("@idScheduleFK", orderDetail.Schedule_Id);
                        command.Parameters.AddWithValue("@idCourseFK", orderDetail.Course_id);


                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }
        //public bool Update(int idInvoice, Invoice invoice)
        //{
        //    bool result = false;

        //    //Sudah Bayar = 0
        //    //Belum Bayar = 1


        //    string query = "UPDATE invoice SET isPaid = @isPaid WHERE idInvoice = @idInvoice";

        //    using (MySqlConnection connection = OpenConnection())
        //    {
        //        try
        //        {
        //            using (MySqlCommand command = new MySqlCommand(query, connection))
        //            {
        //                command.Connection = connection;
        //                command.Parameters.Clear();

        //                command.CommandText = query;
        //                command.Parameters.AddWithValue("@isPaid", invoice.isPaid);
        //                command.Parameters.AddWithValue("@idInvoice", idInvoice);

        //                result = command.ExecuteNonQuery() > 0 ? true : false;

        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //    return result;
        //}


    //    public bool Delete(int id)
    //    {
    //        bool result = false;

    //        string query = $"DELETE FROM invoiceDetail WHERE id = @id";

    //        using (MySqlConnection connection = OpenConnection())
    //        {
    //            try
    //            {
    //                using (MySqlCommand command = new MySqlCommand())
    //                {
    //                    command.Connection = connection;
    //                    command.Parameters.Clear();

    //                    command.CommandText = query;
    //                    command.Parameters.AddWithValue("@id", id);

    //                    result = command.ExecuteNonQuery() > 0 ? true : false;

    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                throw ex;
    //            }
    //            finally
    //            {
    //                connection.Close();
    //            }
    //        }

    //        return result;
    //    }

    }
}
