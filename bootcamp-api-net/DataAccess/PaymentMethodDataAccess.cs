﻿    using bootcamp_api_net.Models;
    using databaseapi.Model;
    using MySql.Data.MySqlClient;
using System.Data;

    namespace bootcamp_api_net.DataAccess
    {
        public class PaymentMethodDataAccess
        {
            private readonly string _connectionString; //"server=localhost;port=3306;database=databaseapi;user=root;password= ;pooling = false; convert zero datetime=True";
            private readonly IConfiguration _configuration;
            private MySqlConnection conn;

            public PaymentMethodDataAccess(IConfiguration configuration)
            {
                _configuration = configuration;
                _connectionString = configuration.GetConnectionString("DefaultConnection");
            }

            private MySqlConnection OpenConnection()
            {
                MySqlConnection connection = new MySqlConnection(_connectionString);
                connection.Open();
                return connection;
            }

            public List<PaymentMethod> GetAll()
            {
                List<PaymentMethod> payment = new List<PaymentMethod>();

                string query = "Select * from paymentmethod";

                using (conn = new MySqlConnection(_connectionString)) ;
                {
                    using (MySqlCommand command = new MySqlCommand(query, conn))
                    {
                        try
                        {
                            conn.Open();

                            using (MySqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    payment.Add(new PaymentMethod
                                    {
                                        // for invoice
                                        Id = Convert.ToInt32(reader["id"]),
                                        Name = (reader["name"].ToString() ?? string.Empty),
                                        Logo = ((byte[])reader["logo"]),
                                        Status = Convert.ToInt32(reader["status"]),
                                        Created = Convert.ToDateTime(reader["Created"]),
                                        Updated = Convert.ToDateTime(reader["Updated"]),
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }

                }

                return payment;
            }

            public PaymentMethod GetById(int id)
            {
                PaymentMethod paymentMethod = null;

                string query = $"Select * from paymentmethod where id = @Id";

                using (conn = new MySqlConnection(_connectionString)) ;
                {
                    using (MySqlCommand command = new MySqlCommand(query, conn))
                    {
                        command.Connection = conn;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", id);

                        try
                        {
                            conn.Open();

                            using (MySqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {

                                    paymentMethod = new PaymentMethod
                                    {
                                        Id = Convert.ToInt32(reader["id"]),
                                        Name = (reader["name"].ToString() ?? string.Empty),
                                        Logo = ((byte[])reader["logo"]),
                                        Status = Convert.ToInt32(reader["status"]),
                                        Created = Convert.ToDateTime(reader["Created"]),
                                        Updated = Convert.ToDateTime(reader["Updated"]),

                                    };


                                }
                            }
                        }
                        catch (Exception ex) { throw ex; }
                        finally { conn.Close(); }
                    }
                }
                return paymentMethod;
            }

            public bool Insert(PaymentMethod paymentMethod)
            {
                bool result = false;
                string date = paymentMethod.Created.ToString("yyyy-MM-dd HH:mm:ss");

                // untuk invoice
                // sudah bayar (true) = 0
                // Jika belum (false) = 1
                string query = $"INSERT INTO paymentmethod(id,name, logo, status,Created, Updated) " +
                  $"VALUES (@Id, @Name, @Logo, @Status, @Created, @Updated)";
                using (MySqlConnection connection = OpenConnection())
                {
                    try
                    {
                        // insert invoice
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            command.Connection = connection;
                            command.Parameters.Clear();

                            command.CommandText = query;
                            command.Parameters.AddWithValue("@Id", paymentMethod.Id);
                            command.Parameters.AddWithValue("@Name", paymentMethod.Name);
                            command.Parameters.AddWithValue("@Logo", paymentMethod.Logo);
                            command.Parameters.AddWithValue("@Status", paymentMethod.Status);
                            command.Parameters.AddWithValue("@Created", date);
                            command.Parameters.AddWithValue("@Updated", date);
                            result = command.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return result;
            }

            public bool UpdatePayment(int id, PaymentMethod payment)    
            {
                bool result = false;
                string query = "UPDATE paymentmethod SET name = @Name , logo = @Logo , status = @Status, " +
                               "Updated = @Updated " +
                               "WHERE id = @Id";

                using (MySqlConnection connection = OpenConnection())
                {
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@Id", id);
                            command.Parameters.AddWithValue("@Name", payment.Name);
                            command.Parameters.AddWithValue("@Logo", payment.Logo);
                            command.Parameters.AddWithValue("@Status", payment.Status);
                            command.Parameters.AddWithValue("@Updated", payment.Updated);

                            result = command.ExecuteNonQuery() > 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception here, e.g., log it.
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return result;
            }

            public bool UpdateActivePayment(int id, PaymentMethod payment)
            {
                bool result = false;
                string query = "UPDATE paymentmethod SET status = 1, Updated = @Updated " +
                               "WHERE id = @Id";

                using (MySqlConnection connection = OpenConnection())
                {
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@Id", id);
                            command.Parameters.AddWithValue("@Updated", payment.Updated);
                            result = command.ExecuteNonQuery() > 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception here, e.g., log it.
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return result;
            }

            public bool UpdateNotActivePayment(int id, PaymentMethod payment)
            {
                bool result = false;
                string query = "UPDATE paymentmethod SET status = 0, Updated = @Updated " +
                               "WHERE id = @Id";

                using (MySqlConnection connection = OpenConnection())
                {
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@Id", id);
                            command.Parameters.AddWithValue("@Updated", payment.Updated);
                            result = command.ExecuteNonQuery() > 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception here, e.g., log it.
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return result;
            }

            public bool DeletePayment(int id)
            {
                bool result = false;
                string query = "DELETE FROM paymentmethod WHERE id = @Id";

                using (MySqlConnection connection = OpenConnection())
                {
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@Id", id);

                            result = command.ExecuteNonQuery() > 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception here, e.g., log it.
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return result;
            }
        public List<PaymentMethod> GetActivePaymentMethods()
        {
            List<PaymentMethod> activePayments = new List<PaymentMethod>();

            string query = "SELECT * FROM paymentmethod WHERE status = 1";

            using (conn = OpenConnection())
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                using (MySqlCommand command = new MySqlCommand(query, conn))
                {
                    try
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                activePayments.Add(new PaymentMethod
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Name = (reader["name"].ToString() ?? string.Empty),
                                    Logo = ((byte[])reader["logo"]),
                                    Status = Convert.ToInt32(reader["status"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"]),
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle the exception here, e.g., log it.
                        throw ex;
                    }
                }
            }

            return activePayments;
        }


    }
}
