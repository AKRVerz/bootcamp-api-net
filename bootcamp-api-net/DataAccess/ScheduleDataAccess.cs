﻿using bootcamp_api_net.Models;
using databaseapi.Model;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace bootcamp_api_net.DataAccess
{
    public class ScheduleDataAccess
    {
        private readonly string _connectionString;

        public ScheduleDataAccess(IConfiguration configuration) 
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private MySqlConnection OpenConnection()
        {
            MySqlConnection connection = new MySqlConnection(_connectionString);
            connection.Open();
            return connection;
        }

        public List<Schedule> GetAll()
        {
            List<Schedule> jadwal = new List<Schedule>();
            string query = "SELECT * FROM schedule";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                jadwal.Add(new Schedule
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    courseFK = Guid.Parse(reader["course_id"].ToString() ?? string.Empty)
                                    //Schedule = Convert.ToDateTime(reader["Schedule"]),
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return jadwal;
        }

        public Schedule GetById(int id)
        {
            Schedule jadwal = null;
            string query = "SELECT * FROM schedule " +
                "inner join courses on schedule.course_id = courses.IdCourse WHERE schedule.id = @id";

            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                jadwal = new Schedule
                                {
                                    Id = Convert.ToInt32(reader["id"]),
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    courseFK = Guid.Parse(reader["course_id"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString(),
                                    Description = reader["deskripsi"].ToString()
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return jadwal;
        }

        public bool Insert(Schedule jadwal)
        {
            bool result = false;
            string query = "INSERT INTO schedule (schedule,course_id) " +
                           "VALUES (@schedule,@course_id)";
            string date = jadwal.Jadwal.ToString("yyyy-MM-dd");
            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@schedule", date);
                        command.Parameters.AddWithValue("course_id", jadwal.courseFK);
                        //command.Parameters.AddWithValue("@Schedule", course.Schedule);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }
    }
}
