﻿using bootcamp_api_net.Models;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;



namespace bootcamp_api_net.DataAccess
{
    public class DataCourseAccess
    {
        private readonly string _connectionString;



        public DataCourseAccess(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }



        private MySqlConnection OpenConnection()
        {
            MySqlConnection connection = new MySqlConnection(_connectionString);
            connection.Open();
            return connection;
        }





        public List<DataCourse> GetAllCourses()
        {
            List<DataCourse> courses = new List<DataCourse>();
            string query = "SELECT * FROM Courses";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                courses.Add(new DataCourse
                                {
                                    Id = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    //Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    Description = (reader["deskripsi"]).ToString() ?? string.Empty,
                                    //Schedule = Convert.ToDateTime(reader["Schedule"]),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString() ?? string.Empty),
                                    ImagePath = (byte[])reader["ImagePath"],
                                    isActive = Convert.ToInt32((reader["isActive"])),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return courses;
        }
        public List<DataCourse> GetAllCoursesWithSchedule()
        {
            List<DataCourse> courses = new List<DataCourse>();
            string query = "SELECT * FROM Courses inner join schedule on Courses.IdCourse = schedule.course_id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                courses.Add(new DataCourse
                                {
                                    Id = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    Description = (reader["deskripsi"]).ToString() ?? string.Empty,
                                    //Schedule = Convert.ToDateTime(reader["Schedule"]),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString() ?? string.Empty),
                                    ImagePath = (byte[])reader["ImagePath"],
                                    isActive = Convert.ToInt32((reader["isActive"])),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return courses;
        }



        //Get All data by id
        public List<DataCourse> GetAllCourseById(Guid id)
        {
            List<DataCourse> course = new List<DataCourse>();
            string query = "SELECT * FROM Courses inner join schedule on Courses.IdCourse = schedule.course_id WHERE Courses.IdCourse = @Id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);



                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course.Add(new DataCourse
                                {
                                    Id = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Description = (reader["deskripsi"]).ToString() ?? string.Empty,
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    //Schedule = Convert.ToDateTime(reader["Schedule"]),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString() ?? string.Empty),
                                    ImagePath = (byte[])reader["ImagePath"],
                                    isActive = Convert.ToInt32((reader["isActive"])),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return course;
        }



        //Get 1 Data By Id
        public DataCourse GetCourseById(Guid id)
        {
            DataCourse course = null;
            string query = "SELECT * FROM Courses inner join schedule on Courses.IdCourse = schedule.course_id WHERE Courses.IdCourse = @Id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);



                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course = new DataCourse
                                {
                                    Id = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Description = (reader["deskripsi"]).ToString() ?? string.Empty,
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    //Schedule = Convert.ToDateTime(reader["Schedule"]),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString() ?? string.Empty),
                                    ImagePath = (byte[])reader["ImagePath"],
                                    isActive = Convert.ToInt32((reader["isActive"])),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return course;
        }



        public DataCourse GetCourseByTitle(string title)
        {
            DataCourse course = null;
            string query = "SELECT * FROM Courses WHERE Title = @Title";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        title = title.ToLower();
                        command.Parameters.AddWithValue("@Title", title);



                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course = new DataCourse
                                {
                                    Id = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Description = (reader["deskripsi"]).ToString() ?? string.Empty,
                                    Price = Convert.ToInt32(reader["Price"]),
                                    //Schedule = Convert.ToDateTime(reader["Schedule"]),
                                    MerkId = Guid.Parse(reader["MerkId"].ToString() ?? string.Empty),
                                    isActive = Convert.ToInt32((reader["isActive"])),
                                    ImagePath = (byte[])reader["ImagePath"],
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return course;
        }



        public bool InsertCourse(DataCourse course)
        {
            bool result = false;



            string query = "INSERT INTO Courses (IdCourse, Title, deskripsi, Price, MerkId, ImagePath, isActive, Created, Updated) " +
                           "VALUES (@Id, @Title, @deskripsi, @Price, @MerkId, @ImagePath, @IsActive, @Created, @Updated)";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", course.Id);
                        command.Parameters.AddWithValue("@Title", course.Title);
                        command.Parameters.AddWithValue("@deskripsi", course.Description);
                        command.Parameters.AddWithValue("@Price", course.Price);
                        //command.Parameters.AddWithValue("@Schedule", course.Schedule);
                        command.Parameters.AddWithValue("@MerkId", course.MerkId);
                        command.Parameters.AddWithValue("@ImagePath", course.ImagePath);
                        command.Parameters.AddWithValue("@IsActive", course.isActive);
                        command.Parameters.AddWithValue("@Created", course.Created);
                        command.Parameters.AddWithValue("@Updated", course.Updated);



                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return result;
        }



        public bool UpdateCourse(Guid id, DataCourse course)
        {
            bool result = false;
            string query = "UPDATE Courses SET Title = @Title, deskripsi = @deskripsi , Price = @Price," +
                " MerkId = @MerkId, ImagePath = @ImagePath, isActive = @IsActive, Updated = @Updated " +
                           "WHERE IdCourse = @Id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        command.Parameters.AddWithValue("@Title", course.Title);
                        command.Parameters.AddWithValue("@deskripsi", course.Description);
                        command.Parameters.AddWithValue("@Price", course.Price);
                        //command.Parameters.AddWithValue("@Schedule", course.Schedule);
                        command.Parameters.AddWithValue("@MerkId", course.MerkId);
                        command.Parameters.AddWithValue("@ImagePath", course.ImagePath);
                        command.Parameters.AddWithValue("@IsActive", course.isActive);
                        command.Parameters.AddWithValue("@Updated", course.Updated);



                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return result;
        }



        public bool UpdateActiveCourse(Guid id, DataCourse course)
        {
            bool result = false;
            string query = "UPDATE Courses SET isActive = 1, Updated = @Updated " +
                           "WHERE IdCourse = @Id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        command.Parameters.AddWithValue("@Updated", course.Updated);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return result;
        }



        public bool UpdateNotActiveCourse(Guid id, DataCourse course)
        {
            bool result = false;
            string query = "UPDATE Courses SET isActive = 0, Updated = @Updated " +
                           "WHERE IdCourse = @Id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        command.Parameters.AddWithValue("@Updated", course.Updated);
                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return result;
        }





        public bool DeleteCourse(Guid id)
        {
            bool result = false;
            string query = "DELETE FROM Courses WHERE IdCourse = @Id";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);



                        result = command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return result;
        }
        public List<DataCourse> GetActiveCourses()
        {
            List<DataCourse> activeCourses = new List<DataCourse>();
            string query = "SELECT * FROM Courses INNER JOIN schedule ON Courses.IdCourse = schedule.course_id WHERE Courses.isActive = 1";



            using (MySqlConnection connection = OpenConnection())
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                activeCourses.Add(new DataCourse
                                {
                                    Id = Guid.Parse(reader["IdCourse"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Jadwal = DateOnly.FromDateTime(Convert.ToDateTime(reader["schedule"])),
                                    Price = Convert.ToInt32(reader["Price"]),
                                    Description = (reader["deskripsi"]).ToString() ?? string.Empty,
                                    MerkId = Guid.Parse(reader["MerkId"].ToString() ?? string.Empty),
                                    ImagePath = (byte[])reader["ImagePath"],
                                    isActive = Convert.ToInt32((reader["isActive"])),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception here, e.g., log it.
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }



            return activeCourses;
        }
    }
}