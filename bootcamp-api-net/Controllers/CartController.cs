﻿using bootcamp_api_net.DataAccess;
using bootcamp_api_net.DTOs.Cart;
using bootcamp_api_net.DTOs.Course;
using bootcamp_api_net.Models;
using Microsoft.AspNetCore.Mvc;

namespace bootcamp_api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : Controller
    {
        private readonly CartDataAcess _cartAccess;

        public CartController(CartDataAcess cartDataAcess)
        {
            _cartAccess = cartDataAcess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var courses = _cartAccess.GetAllCart();
                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetByUserId(Guid id)
        {
            try
            {
                var cartItems = _cartAccess.GetCartItemsByUserId(id);
                return Ok(cartItems);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }
        
        [HttpPost]
        public IActionResult Post([FromBody] CartDTO cartDTO)
        {
            try
            {
                if (cartDTO == null)
                    return BadRequest("Data should be provided");
                
                Cart cart = new Cart()
                {
                    Id_User = cartDTO.Id_User,
                    Id_Schedule = cartDTO.Id_Schedule,
                };

                //List<Cart> cart =

                bool result = _cartAccess.InsertCart(cart);

                if (result)
                {
                    return StatusCode(201, cart.Id);
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int[] id)
        {
            try
            {
                bool result = _cartAccess.DeleteCart(id);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        
        [HttpDelete("DeleteAllCartByUser/{id}")]
        public IActionResult DeleteAll(Guid id)
        {
            try
            {
                bool result = _cartAccess.DeleteAllCartbyUser(id);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }
    }
}
