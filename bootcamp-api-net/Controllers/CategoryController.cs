using databaseapi.DataAccess;
using databaseapi.DTOs.Category;
using databaseapi.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace databaseapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryDataAccess _categoryDataAccess;

        public CategoryController(CategoryDataAccess categoryDataAccess)
        {
            _categoryDataAccess = categoryDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var categories = _categoryDataAccess.GetAllCategories();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpGet("isActive")]
        public IActionResult GetAllActive()
        {
            try
            {
                var categories = _categoryDataAccess.GetAllCategoriesActive();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                Category category = _categoryDataAccess.GetCategoryById(id);

                if (category == null)
                {
                    return NotFound("Data not found");
                }

                return Ok(category);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] CategoryDTO categoryDto)
        {
            try
            {
                if (categoryDto == null)
                    return BadRequest("Data should be inputted");

                Category category = new Category
                {
                    Id = Guid.NewGuid(),
                    CategoryName = categoryDto.CategoryName,
                    deskripsi = categoryDto.deskripsi,
                    ImagePath = categoryDto.ImagePath,
                    isActive = categoryDto.isActive ? 1 : 0 // Convert boolean isActive to 1 or 0
                };

                bool result = _categoryDataAccess.InsertCategory(category);

                if (result)
                {
                    return StatusCode(201, category.Id);
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] CategoryDTO categoryDto)
        {
            try
            {
                if (categoryDto == null)
                    return BadRequest("Data should be inputted");

                Category category = new Category
                {
                    CategoryName = categoryDto.CategoryName,
                    deskripsi = categoryDto.deskripsi,
                    ImagePath = categoryDto.ImagePath,
                    isActive = categoryDto.isActive ? 1 : 0 // Convert boolean isActive to 1 or 0
                };

                bool result = _categoryDataAccess.UpdateCategory(id, category);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("putActive/{id}")]
        public IActionResult PutActive(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be inputted");

                bool result = _categoryDataAccess.ChangeCategoryStatus(id, true);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("putNotActive/{id}")]
        public IActionResult PutNotActive(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be inputted");

                bool result = _categoryDataAccess.ChangeCategoryStatus(id, false);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                bool result = _categoryDataAccess.DeleteCategory(id);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpGet("active")]
        public IActionResult GetActiveCategories()
        {
            try
            {
                var activeCategories = _categoryDataAccess.GetActiveCategories();
                return Ok(activeCategories);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

    }
}
