﻿using bootcamp_api_net.DataAccess;
using bootcamp_api_net.DTOs.Course;
using bootcamp_api_net.Models;
using databaseapi.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;



namespace bootcamp_api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly DataCourseAccess _courseAccess;



        public CourseController(DataCourseAccess dataCourseAccess)
        {
            _courseAccess = dataCourseAccess;
        }



        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var courses = _courseAccess.GetAllCourses();
                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpGet("GetJadwalWithCourse")]
        public IActionResult GetAllCourseWithSchedule()
        {
            try
            {
                var courses = _courseAccess.GetAllCoursesWithSchedule();
                return Ok(courses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            //try
            //{
            //    List<DataCourse> course = _courseAccess.GetAllCourseById(id);



            //    if (course == null)
            //    {
            //        return NotFound("Data not found");
            //    }



            //    return Ok(course);
            //}
            //catch (Exception ex)
            //{
            //    return StatusCode(500, $"Error: {ex.Message}");
            //}



            List<DataCourse> course = _courseAccess.GetAllCourseById(id);



            if (course == null)
            {
                return NotFound("Data not found");
            }



            return Ok(course);
        }



        [HttpGet("GetByTitle/{title}")]
        public IActionResult GetByTitle(string title)
        {
            try
            {
                DataCourse course = _courseAccess.GetCourseByTitle(title);



                if (course == null)
                {
                    return NotFound("Data not found");
                }



                return Ok(course);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpPost]
        public IActionResult Post([FromBody] CourseDTO courseDTO)
        {
            int cek = 0;
            if (courseDTO.isActive == false)
            {
                cek = 0;
            }
            else
            {
                cek = 1;
            }
            try
            {
                if (courseDTO == null)
                    return BadRequest("Data should be provided");



                DataCourse course = new DataCourse()
                {
                    Id = Guid.NewGuid(),
                    Title = courseDTO.Title,
                    Description = courseDTO.Description,
                    Price = courseDTO.Price,
                    MerkId = courseDTO.MerkId,
                    ImagePath = courseDTO.ImagePath,
                    //Schedule = courseDTO.Schedule,
                    isActive = cek,
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                };



                bool result = _courseAccess.InsertCourse(course);



                if (result)
                {
                    return StatusCode(201, course.Id);
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] CourseDTO courseDTO)
        {
            int cek = 0;
            if (courseDTO.isActive == false)
            {
                cek = 0;
            }
            else
            {
                cek = 1;
            }
            try
            {
                if (courseDTO == null)
                    return BadRequest("Data should be provided");



                DataCourse existingCourse = _courseAccess.GetCourseById(id);



                if (existingCourse == null)
                {
                    return NotFound("Data not found");
                }



                existingCourse.Title = courseDTO.Title;
                existingCourse.MerkId = courseDTO.MerkId;
                existingCourse.Description = courseDTO.Description;
                existingCourse.Price = courseDTO.Price;
                existingCourse.ImagePath = courseDTO.ImagePath;
                //existingCourse.Schedule = courseDTO.Schedule;
                existingCourse.isActive = cek;
                existingCourse.Updated = DateTime.Now;



                bool result = _courseAccess.UpdateCourse(id, existingCourse);



                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpPut("updateActiveCourse/{id}")]
        public IActionResult PutActive(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be provided");



                DataCourse existingCourse = _courseAccess.GetCourseById(id);



                if (existingCourse == null)
                {
                    return NotFound("Data not found");
                }



                //existingCourse.Title = courseDTO.Title;
                //existingCourse.MerkId = courseDTO.MerkId;
                //existingCourse.Description = courseDTO.Description;
                //existingCourse.Price = courseDTO.Price;
                //existingCourse.Schedule = courseDTO.Schedule;
                //existingCourse.isActive = cek;
                existingCourse.Updated = DateTime.Now;



                bool result = _courseAccess.UpdateActiveCourse(id, existingCourse);



                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpPut("updateNotActiveCourse/{id}")]
        public IActionResult PutNotActive(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be provided");



                DataCourse existingCourse = _courseAccess.GetCourseById(id);



                if (existingCourse == null)
                {
                    return NotFound("Data not found");
                }



                //existingCourse.Title = courseDTO.Title;
                //existingCourse.MerkId = courseDTO.MerkId;
                //existingCourse.Description = courseDTO.Description;
                //existingCourse.Price = courseDTO.Price;
                //existingCourse.Schedule = courseDTO.Schedule;
                //existingCourse.isActive = cek;
                existingCourse.Updated = DateTime.Now;



                bool result = _courseAccess.UpdateNotActiveCourse(id, existingCourse);



                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }



        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                bool result = _courseAccess.DeleteCourse(id);



                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }





        [HttpGet("Active")]
        public IActionResult GetActiveCourses()
        {
            try
            {
                var activeCourses = _courseAccess.GetActiveCourses(); // Metode untuk mengambil data course yang aktif dari database
                return Ok(activeCourses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }
    }
}