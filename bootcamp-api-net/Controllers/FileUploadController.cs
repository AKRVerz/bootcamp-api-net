﻿using BookFS10.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookFS10.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadController : Controller
    {
        public static IWebHostEnvironment _webHostEnvironment;

        public FileUploadController(IWebHostEnvironment webHostEnvironment) 
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost]
        public async Task<string> Post([FromForm] FileUpload file)
        {
            try 
            {
                if (file.files.Length > 0)
                {
                    string path = _webHostEnvironment.WebRootPath + "\\Uploads\\Product\\";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    using (FileStream fileStream = System.IO.File.Create(path + file.files.FileName))
                    {
                        file.files.CopyTo(fileStream);
                        fileStream.Flush();
                        return "Upload Done";
                    }
                }
                else 
                {
                    return "Failed.";
                }
            }
            catch (Exception ex) 
            {
                return ex.Message;
            }
        }

        [HttpGet("{fileName}")]
        public async Task<IActionResult> Get([FromRoute] string fileName) 
        {
            string path = _webHostEnvironment.WebRootPath + "\\Uploads\\Product\\";
            string notPath = _webHostEnvironment.WebRootPath + "\\Uploads\\Common\\";
            var filePath = path + fileName + ".png";
            var notFilePath = notPath + fileName +".png";
            try 
            {

                if (System.IO.File.Exists(filePath))
                {
                    byte[] b = System.IO.File.ReadAllBytes(filePath);
                    return File(b, "image/png");
                }
                else 
                {
                    byte[] c = System.IO.File.ReadAllBytes(notFilePath);
                    return File(c, "image/png");
                }
               //return StatusCode(StatusCodes.Status201Created);
            }
            catch (Exception) 
            {
                return StatusCode(StatusCodes.Status404NotFound);
                //return StatusCode(StatusCodes.Status404NotFound);
            }
            //byte[] c = System.IO.File.ReadAllBytes(notFilePath);
            //return File(c, "image/png");
        }
    }
}
