﻿using bootcamp_api_net.DataAccess;
using bootcamp_api_net.DTOs.Schedule;
using bootcamp_api_net.Models;
using databaseapi.DataAccess;
using databaseapi.DTOs.Invoice;
using databaseapi.Model;
using Microsoft.AspNetCore.Mvc;

namespace bootcamp_api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : Controller
    {
        private readonly ScheduleDataAccess _scheduleDataAccess;
       
        public ScheduleController(ScheduleDataAccess scheduleDataAccess)
        {
            _scheduleDataAccess = scheduleDataAccess;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var jadwal = _scheduleDataAccess.GetAll();
            return Ok(jadwal);
        }

        [HttpGet("GetById")]
        public IActionResult Get(int id) 
        {
            Schedule jadwal = _scheduleDataAccess.GetById(id);
            if (jadwal==null) 
            {
                return NotFound("Data not found");
            }
            return Ok(jadwal);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ScheduleDTO scheduleDTO) 
        {
            if (scheduleDTO == null)
                return BadRequest("Data should be inputed");

            Schedule jadwal = new Schedule
            {
                Jadwal = scheduleDTO.Jadwal,
                courseFK = scheduleDTO.courseFK
            };

            bool result = _scheduleDataAccess.Insert(jadwal);

            if (result)
            {
                return StatusCode(201, jadwal.Jadwal);
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }
    }
}
