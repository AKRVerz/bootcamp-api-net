﻿using bootcamp_api_net.DataAccess;
using bootcamp_api_net.DTOs.Course;
using bootcamp_api_net.DTOs.Payment;
using bootcamp_api_net.Models;
using databaseapi.DataAccess;
using databaseapi.DTOs.Invoice;
using databaseapi.Model;
using Microsoft.AspNetCore.Mvc;

namespace bootcamp_api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentMethodController : Controller
    {
        
        private readonly PaymentMethodDataAccess _paymentMethodDataAccess;

        public PaymentMethodController(PaymentMethodDataAccess paymentMethodDataAccess)
        {
            _paymentMethodDataAccess = paymentMethodDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var payment = _paymentMethodDataAccess.GetAll();
            return Ok(payment);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            PaymentMethod payment = _paymentMethodDataAccess.GetById(id);

            if (payment == null)
            {
                return NotFound("Data not found");
            }

            return Ok(payment);
        }

        [HttpPost]
        public IActionResult Post([FromBody] PaymentMethodDTO paymentMethodDTO)
        {
            if (paymentMethodDTO == null)
                return BadRequest("Data should be inputed");

            // Untuk IsPaid
            // 0 = sudah bayar
            // 1 = belum bayar
            int cek = 0;
            if (paymentMethodDTO.Status==false)
            {
                cek = 0;
            }
            else
            {
                cek = 1;
            }

            PaymentMethod paymentMethod = new PaymentMethod
            {
                Name = paymentMethodDTO.Name,
                Logo = paymentMethodDTO.Logo,
                Status = cek,
                Created = DateTime.Now,
                Updated = DateTime.Now,
            };

            bool result = _paymentMethodDataAccess.Insert(paymentMethod);

            if (result)
            {
                return StatusCode(201, paymentMethod.Id);
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] PaymentMethodDTO paymentMethodDTO)
        {
            int cek = 0;
            if (paymentMethodDTO.Status == false)
            {
                cek = 0;
            }
            else
            {
                cek = 1;
            }
            try
            {
                if (paymentMethodDTO == null)
                    return BadRequest("Data should be provided");

                PaymentMethod existingPayment = _paymentMethodDataAccess.GetById(id);

                if (existingPayment == null)
                {
                    return NotFound("Data not found");
                }

                existingPayment.Name = paymentMethodDTO.Name;
                existingPayment.Logo = paymentMethodDTO.Logo;
                //existingCourse.Schedule = courseDTO.Schedule;
                existingPayment.Status = cek;
                existingPayment.Updated = DateTime.Now;

                bool result = _paymentMethodDataAccess.UpdatePayment(id, existingPayment);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("updateActiveCourse/{id}")]
        public IActionResult PutActive(int id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be provided");

                PaymentMethod existingPayment = _paymentMethodDataAccess.GetById(id);

                if (existingPayment == null)
                {
                    return NotFound("Data not found");
                }

                existingPayment.Updated = DateTime.Now;

                bool result = _paymentMethodDataAccess.UpdateActivePayment(id, existingPayment);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("updateNotActiveCourse/{id}")]
        public IActionResult PutNotActive(int id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be provided");

                PaymentMethod existingPayment = _paymentMethodDataAccess.GetById(id);

                if (existingPayment == null)
                {
                    return NotFound("Data not found");
                }

                existingPayment.Updated = DateTime.Now;

                bool result = _paymentMethodDataAccess.UpdateNotActivePayment(id, existingPayment);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                bool result = _paymentMethodDataAccess.DeletePayment(id);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }
        [HttpGet("GetActivePaymentMethods")]
        public IActionResult GetActivePaymentMethods()
        {
            var activePayments = _paymentMethodDataAccess.GetActivePaymentMethods();

            if (activePayments == null || activePayments.Count == 0)
            {
                return NotFound("No active payment methods found.");
            }

            return Ok(activePayments);
        }

    }
}
