﻿using bootcamp_api_net.Emails;
using bootcamp_api_net.Emails.Template;
using bootcamp_api_net.DataAccess;
using bootcamp_api_net.DTOs.User;
using bootcamp_api_net.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using bootcamp_api_net.DTOs.Payment;

namespace bootcamp_api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserDataAccess _userData;
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;

        public UserController(UserDataAccess userData, IConfiguration configuration, EmailService emailService)
        {
            _userData = userData;
            _configuration = configuration;
            _emailService = emailService;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var users = _userData.GetAllUsers();
                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            List<User> users = _userData.GetAllUsersById(id);
            if (users == null)
            {
                return NotFound("Data Tidak ada");
            }

            return Ok(users);
        }

        [HttpGet("GetByUsername/{username}")]
        public IActionResult GetByUsername(string username)
        {
            try
            {
                User user = _userData.GetCourseByUsername(username);

                if (user == null)
                {
                    return NotFound("Data not found");
                }

                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody] UserDTO userDto)
        {
            try
            {
                User user = new User
                {
                    Id = Guid.NewGuid(),
                    Username = userDto.Username,
                    Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password),
                    Email = userDto.Email,
                    IsActivated = false
                };

                UserRole userRole = new UserRole
                {
                    UserId = user.Id,
                    Role = userDto.Role
                };

                bool result = _userData.CreateUserAccount(user, userRole);

                if (result)
                {
                    bool mailResult = await SendMailActivation(user);
                    return StatusCode(201, userDto);
                }
                else
                {
                    return StatusCode(500, "Data not inserted");
                }
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginRequestDTO credential)
        {
            if (credential is null)
                return BadRequest("Invalid client request");

            if (string.IsNullOrEmpty(credential.Username) || string.IsNullOrEmpty(credential.Password))
                return BadRequest("Invalid client request");

            User? user = _userData.CheckUser(credential.Username);

            if (user == null)
                return Unauthorized("You do not authorized");

            if (user.IsActivated != true)
            {
                return Unauthorized("Your account has not activated");
            }

            UserRole? userRole = _userData.GetUserRole(user.Id);


            bool isVerified = BCrypt.Net.BCrypt.Verify(credential.Password, user.Password);

            if (user != null && !isVerified)
            {
                return BadRequest("Incorrect Password! Please check your password!");
            }
            else
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("JwtConfig:Key").Value));

                var claims = new Claim[] {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Role, userRole.Role)
                };

                var signingCredential = new SigningCredentials(
                    key, SecurityAlgorithms.HmacSha256Signature);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddMinutes(10),
                    SigningCredentials = signingCredential
                };

                var tokenHandler = new JwtSecurityTokenHandler();

                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                string token = tokenHandler.WriteToken(securityToken);

                return Ok(new LoginResponseDTO { Token = token });
            }
        }

        [HttpGet("ActivateUser")]
        public IActionResult ActivateUser(Guid userId, string username)
        {
            try
            {
                User? user = _userData.CheckUser(username);

                if (user == null)
                    return BadRequest("Activation Failed");

                if (user.IsActivated == true)
                    return BadRequest("Account has been activated");

                bool result = _userData.AcitvateUser(userId);

                if (result)
                    return Ok("User Activated Successfully");
                else
                    return BadRequest("Activation Failed");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("SendMailUser")]
        public async Task<IActionResult> SendMailUser([FromBody] string mailTo)
        {
            List<string> to = new List<string>();
            to.Add(mailTo);

            string subject = "Test Email FS10";
            string body = "Hallo, First Email";

            EmailModel model = new EmailModel(to, subject, body);

            bool sendMail = await _emailService.SendAsync(model, new CancellationToken());

            if (sendMail)
                return Ok("Send");
            else
                return StatusCode(500, "Error");
        }

        private async Task<bool> SendMailActivation(User user)
        {
            if (user == null)
                return false;

            if (string.IsNullOrEmpty(user.Email))
                return false;

            List<string> to = new List<string>();
            to.Add(user.Email);

            string subject = "Account Activation";

            var param = new Dictionary<string, string>()
            {
                {"userId", user.Id.ToString() },
                { "username", user.Username }
            };

            string callback = QueryHelpers.AddQueryString("http://localhost:2031/confirm-email", param);

            //string body = "Please confirm account by clicking this <a href=\"" + callback + "\"> Link</a>";

            string body = _emailService.GetMailTemplate("EmailActivation", new ActivationModel()
            {
                Email = user.Email,
                Link = callback
            });

            EmailModel emailModel = new EmailModel(to, subject, body);
            bool mailResult = await _emailService.SendAsync(emailModel, new CancellationToken());

            return mailResult;
        }

        [HttpPost("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                    return BadRequest("Email is empty");

                bool sendMail = await SendEmailForgetPassword(email);

                if (sendMail)
                {
                    return Ok("Mail sent");
                }
                else
                {
                    return StatusCode(500, "Error");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword([FromBody] ResetPasswordDTO resetPassword)
        {
            try
            {
                if (resetPassword == null)
                    return BadRequest("No Data");

                if (resetPassword.Password != resetPassword.ConfirmPassword)
                {
                    return BadRequest("Password doesn't match");
                }

                bool reset = _userData.ResetPassword(resetPassword.Email, BCrypt.Net.BCrypt.HashPassword(resetPassword.Password));

                if (reset)
                {
                    return Ok("Reset password OK");
                }
                else
                {
                    return StatusCode(500, "Error");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        private async Task<bool> SendEmailForgetPassword(string email)
        {
            // send email
            List<string> to = new List<string>();
            to.Add(email);

            string subject = "Forget Password";

            var param = new Dictionary<string, string?>
                    {
                        {"email", email }
                    };

            string callbackUrl = QueryHelpers.AddQueryString("http://localhost:2031/reset-password", param);

            string body = "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>";

            EmailModel mailModel = new EmailModel(to, subject, body);

            bool mailResult = await _emailService.SendAsync(mailModel, new CancellationToken());

            return mailResult;

        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] UserDTO userDTO)
        {
            try
            {
                if (userDTO == null)
                    return BadRequest("Data should be provided");

                User existingUser = _userData.GetUsersById(id);

                if (existingUser == null)
                {
                    return NotFound("Data not found");
                }

                existingUser.Username = userDTO.Username;
                existingUser.Password = BCrypt.Net.BCrypt.HashPassword(userDTO.Password);
                existingUser.Email = userDTO.Email;

                bool result = _userData.UpdateUser(id, existingUser);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("activateUser/{id}")]
        public IActionResult PutActiveUser(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be provided");

                User existingUser = _userData.GetUsersById(id);

                if (existingUser == null)
                {
                    return NotFound("Data not found");
                }

                //existingUser.Username = userDTO.Username;
                //existingUser.Password = userDTO.Password;
                //existingUser.Email = userDTO.Email;

                bool result = _userData.UpdateUserActive(id);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpPut("notActivateUser/{id}")]
        public IActionResult PutNotActiveUser(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest("Data should be provided");

                User existingUser = _userData.GetUsersById(id);

                if (existingUser == null)
                {
                    return NotFound("Data not found");
                }

                //existingUser.Username = userDTO.Username;
                //existingUser.Password = userDTO.Password;
                //existingUser.Email = userDTO.Email;

                bool result = _userData.UpdateUserNotActive(id);

                if (result)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(500, "Error occurred");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [HttpGet("getAllCourseById/{id}")]
        public IActionResult GetAllCourseUser(Guid id)
        {
            List<CourseUser> users = _userData.GetAllCourseUser(id);
            if (users == null)
            {
                return NotFound("Data Tidak ada");
            }

            return Ok(users);
        }

        [HttpGet("getCourseById/{id}")]
        public IActionResult GetCourseUser(Guid id, Guid iDcourse)
        {
            CourseUser users = _userData.GetCourseUser(id,iDcourse);
            if (users == null)
            {
                return NotFound("Data Tidak ada");
            }

            return Ok(users);
        }
    }

}
