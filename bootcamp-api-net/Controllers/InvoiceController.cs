using bootcamp_api_net.DTOs.OrderDetail;
using bootcamp_api_net.Models;
using databaseapi.DataAccess;
using databaseapi.DTOs.Invoice;
using databaseapi.Model;
using Microsoft.AspNetCore.Mvc;

namespace databaseapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceDataAccess _invoiceDataAccess;

        public InvoiceController(InvoiceDataAccess invoiceDataAccess)
        {
            _invoiceDataAccess = invoiceDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var invoice = _invoiceDataAccess.GetAll();
            return Ok(invoice);
        }

        [HttpGet("LastIndex")]
        public IActionResult GetLastIndex()
        {
            var invoice = _invoiceDataAccess.GetLastId();
            return Ok(invoice);
        }

        [HttpGet("GetById")]
        public IActionResult Get(int idInvoice)
        {
            Invoice? invoice = _invoiceDataAccess.GetById(idInvoice);

            if (invoice == null)
            {
                return NotFound("Data not found");
            }

            return Ok(invoice);
        }

        [HttpGet("GetByUser")]
        public IActionResult GetByUser(Guid id)
        {
            List<Invoice> invoice = _invoiceDataAccess.GetAllByUser(id);

            if (invoice == null)
            {
                return NotFound("Data not found");
            }

            return Ok(invoice);
        }

        [HttpGet("GetByIdDetail")]
        public IActionResult GetDetail(Guid UserID, int idInvoice)
        {
            List<OrderDetail> orderDetails = _invoiceDataAccess.GetByIdDetail(UserID, idInvoice);

            if (orderDetails == null)
            {
                return NotFound("Data not found");
            }

            return Ok(orderDetails);
        }

        [HttpPost]
        public IActionResult Post([FromBody] InvoiceDTO invoiceDtO)
        {
            if (invoiceDtO == null)
                return BadRequest("Data should be inputed");

            // Untuk IsPaid
            // 0 = sudah bayar
            // 1 = belum bayar
            int cek = 0;
            if (invoiceDtO.isPaid)
            {
                cek = 1;
            }
            else
            {
                cek = 0;
            }

            Invoice invoice = new Invoice
            {
                noInvoice = invoiceDtO.noInvoice,
                id_User = invoiceDtO.idUserFK,
                date = DateTime.Now,
                payment_method = invoiceDtO.payment_method,
                isPaid = cek
            };

            bool result = _invoiceDataAccess.Insert(invoice);

            if (result)
            {
                return StatusCode(201, invoice.noInvoice);
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [Route("postDetail")]
        [HttpPost]
        public IActionResult PostDetail([FromBody] OrderDetailDTO orderDetailDTO)
        {
            if (orderDetailDTO == null)
                return BadRequest("Data should be inputed");

            // Untuk IsPaid
            // 1 = sudah bayar
            // 0 = belum bayar

            OrderDetail orderDetail = new OrderDetail
            {
                id_User = orderDetailDTO.id_User,
                noInv = orderDetailDTO.noInv,
                Schedule_Id = orderDetailDTO.Schedule_Id,
                Course_id = orderDetailDTO.Course_id,
            };

            bool result = _invoiceDataAccess.InsertDetail(orderDetail);

            if (result)
            {
                return StatusCode(201, orderDetail.noInv);
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }


        //[HttpPut]
        //public IActionResult Put(int idInvoice, [FromBody] InvoicePay invoicePay)
        //{
        //    if (invoicePay == null)
        //        return BadRequest("Data should be inputed");

        //    // Untuk IsPaid
        //    // 0 = sudah bayar
        //    // 1 = belum bayar
        //    int cek = 0;
        //    if (invoicePay.isPaid)
        //    {
        //        cek = 0;
        //    }
        //    else
        //    {
        //        cek = 1;
        //    }

        //    Invoice invoice = new Invoice
        //    {
        //        isPaid = cek,
        //    };

        //    bool result = _invoiceDataAccess.Update(idInvoice, invoice);

        //    if (result)
        //    {
        //        return NoContent();
        //    }
        //    else
        //    {
        //        return StatusCode(500, "Error occur");
        //    }
        //}


        //[HttpDelete]
        //public IActionResult Delete(int id)
        //{
        //    bool result = _invoiceDataAccess.Delete(id);

        //    if (result)
        //    {
        //        return NoContent();
        //    }
        //    else
        //    {
        //        return StatusCode(500, "Error occur");
        //    }
        //}

        


    }
}
